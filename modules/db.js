const config = require('config');
const knex = require('knex')(config.get('knex'));

module.exports = knex;

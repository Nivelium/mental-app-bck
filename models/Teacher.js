const { Model } = require('objection');
const knex = require('../modules/db');
const _ = require('lodash');

Model.knex(knex);

class Teacher extends Model {
    static get tableName() {
        return 'Teacher';
    }

    async update() {
        return Teacher.query().where({ id: this.id }).update(_.omit(this, 'id', 'created_at'));
    }
}

module.exports = Teacher;

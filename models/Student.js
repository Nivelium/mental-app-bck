const { Model } = require('objection');
const knex = require('../modules/db');
const _ = require('lodash');

Model.knex(knex);

class Student extends Model {
  static get tableName() {
    return 'Student';
  }

  async update() {
    return Student.query().where({ id: this.id }).update(_.omit(this, 'id', 'created_at'));
  }
}

module.exports = Student;

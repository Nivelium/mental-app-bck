const { Router } = require('express');
const teacherRouter = new Router();
const Teacher = require('../models/Teacher');
const _ = require('lodash');

/**
 * @api {get} /teacher/:teacherId Get teacher info
 * @apiVersion 0.0.1
 * @apiName GetTeacherById
 * @apiGroup Teacher
 * @apiParam {Number} id Teacher's unique ID.
 */
teacherRouter.get('/teacher/:teacherId', async (req, res) => {
    req.log.info(req.params);
    try {
        const [teacher] = await Teacher.query().where('id', req.params.teacherId);

        req.log.info(teacher);
        res.status(200).send(teacher).end();
    } catch (e) {
        req.log.error(e);
        res.status(404).end();
    }
});
/**
 * @api {post} /teacher Post teacher info
 * @apiVersion 0.0.1
 * @apiName PostTeacher
 * @apiGroup Teacher
 * @apiParam (RequestBody) {String} email     Teacher's email.
 * @apiParam (RequestBody) {String} firstName Teacher's firstName.
 * @apiParam (RequestBody) {String} lastName  Teacher's lastName.
 * @apiParam (RequestBody) {Date}   birthDate Teacher's birthDate.
 * @apiParam (RequestBody) {String} gender    Teacher's gender.
 * @apiParam (RequestBody) {String} phone     Teacher's phone.
 * @apiParam (RequestBody) {String} country   Teacher's country.
 * @apiParam (RequestBody) {String} city      Teacher's city.
 * @apiParam (RequestBody) {String} district  Teacher's district.
 *
 */
teacherRouter.post('/teacher', async (req, res) => {
    const data = req.body;

    req.log.info(data);
    try {
        const { id } = await Teacher.query().insert(data);

        req.log.info(id);
        res.status(201).send({ id }).end();
    } catch (e) {
        req.log.error(e);
        res.status(412).end();
    }
});
/**
 * @api {put} /teacher/:teacherId Update whole teacher's info
 * @apiVersion 0.0.1
 * @apiName PutTeacher
 * @apiGroup Teacher
 * @apiParam (RequestBody) {String} email     Teacher's email.
 * @apiParam (RequestBody) {String} firstName Teacher's firstName.
 * @apiParam (RequestBody) {String} lastName  Teacher's lastName.
 * @apiParam (RequestBody) {Date}   birthDate Teacher's birthDate.
 * @apiParam (RequestBody) {String} gender    Teacher's gender.
 * @apiParam (RequestBody) {String} phone     Teacher's phone.
 * @apiParam (RequestBody) {String} country   Teacher's country.
 * @apiParam (RequestBody) {String} city      Teacher's city.
 * @apiParam (RequestBody) {String} district  Teacher's district.
 *
 */
teacherRouter.put('/teacher/:teacherId', async (req, res) => {
    const data = req.body;
    const [teacher] = await Teacher.query().where('id', req.params.teacherId);
    const teacherPut = _.mergeWith(_.omit(teacher, 'id', 'created_at'), data, (teacherValue, bodyValue) => {
        if (teacherValue === null || teacherValue === undefined) {
            return null;
        }

        return teacherValue;
    });

    if (teacher) {
        await Teacher.query().where({ id: req.params.teacherId }).update(teacherPut);
        res.status(204).end();
    } else {
        const { id } = await Teacher.query().insert(data);

        res.status(201).send({ id }).end();
    }
});
/**
 * @api {patch} /teacher/:teacherId Update partially teacher's info
 * @apiVersion 0.0.1
 * @apiName PatchTeacher
 * @apiGroup Teacher
 * @apiParam (RequestBody) {String} email     Teacher's email.
 * @apiParam (RequestBody) {String} firstName Teacher's firstName.
 * @apiParam (RequestBody) {String} lastName  Teacher's lastName.
 * @apiParam (RequestBody) {Date}   birthDate Teacher's birthDate.
 * @apiParam (RequestBody) {String} gender    Teacher's gender.
 * @apiParam (RequestBody) {String} phone     Teacher's phone.
 * @apiParam (RequestBody) {String} country   Teacher's country.
 * @apiParam (RequestBody) {String} city      Teacher's city.
 * @apiParam (RequestBody) {String} district  Teacher's district.
 *
 */
teacherRouter.patch('/teacher/:teacherId', async (req, res) => {
    const data = req.body;
    const [teacher] = await Teacher.query().where('id', req.params.teacherId);

    if (teacher) {
        await teacher.update(data);
        res.status(204).end();
    } else {
        res.status(412).end();
    }
});
/**
 * @api {delete} /teacher/:teacherId Delete whole teacher's info
 * @apiVersion 0.0.1
 * @apiName DeleteTeacher
 * @apiGroup Teacher
 * @apiParam {String} teacherId Teacher's ID used to delete record.
 *
 */
teacherRouter.delete('/teacher/:teacherId', async (req, res) => {

    res.status(204).end();
});

module.exports = teacherRouter;

const { Router } = require('express');
const studentRouter = new Router();
const Student = require('../models/Student');
const _ = require('lodash');

/**
 * @api {get} /student/:studentId Get student info
 * @apiVersion 0.0.1
 * @apiName GetStudentById
 * @apiGroup Student
 * @apiParam {Number} id Student's unique ID.
 */
studentRouter.get('/student/:studentId', async (req, res) => {
  req.log.info(req.params);
  try {
    const [student] = await Student.query().where('id', req.params.studentId);

    req.log.info(student);
    res.status(200).send(student).end();
  } catch (e) {
    req.log.error(e);
    res.status(404).end();
  }
});
/**
 * @api {post} /student Post student info
 * @apiVersion 0.0.1
 * @apiName PostStudent
 * @apiGroup Student
 * @apiParam (RequestBody) {String} email     Student's email.
 * @apiParam (RequestBody) {String} firstName Student's firstName.
 * @apiParam (RequestBody) {String} lastName  Student's lastName.
 * @apiParam (RequestBody) {Date}   birthDate Student's birthDate.
 * @apiParam (RequestBody) {String} gender    Student's gender.
 * @apiParam (RequestBody) {String} phone     Student's phone.
 * @apiParam (RequestBody) {String} country   Student's country.
 * @apiParam (RequestBody) {String} city      Student's city.
 * @apiParam (RequestBody) {String} district  Student's district.
 *
 */
studentRouter.post('/student', async (req, res) => {
  const data = req.body;

  req.log.info(data);
  try {
    const { id } = await Student.query().insert(data);

    req.log.info(id);
    res.status(201).send({ id }).end();
  } catch (e) {
    req.log.error(e);
    res.status(412).end();
  }
});
/**
 * @api {put} /student/:studentId Update whole student's info
 * @apiVersion 0.0.1
 * @apiName PutStudent
 * @apiGroup Student
 * @apiParam (RequestBody) {String} email     Student's email.
 * @apiParam (RequestBody) {String} firstName Student's firstName.
 * @apiParam (RequestBody) {String} lastName  Student's lastName.
 * @apiParam (RequestBody) {Date}   birthDate Student's birthDate.
 * @apiParam (RequestBody) {String} gender    Student's gender.
 * @apiParam (RequestBody) {String} phone     Student's phone.
 * @apiParam (RequestBody) {String} country   Student's country.
 * @apiParam (RequestBody) {String} city      Student's city.
 * @apiParam (RequestBody) {String} district  Student's district.
 *
 */
studentRouter.put('/student/:studentId', async (req, res) => {
  const data = req.body;
  const [student] = await Student.query().where('id', req.params.studentId);
  const studentPut = _.mergeWith(_.omit(student, 'id', 'created_at'), data, (studentValue, bodyValue) => {
    if (studentValue === null || studentValue === undefined) {
      return null;
    }

    return studentValue;
  });

  if (student) {
    await Student.query().where({ id: req.params.studentId }).update(studentPut);
    res.status(204).end();
  } else {
    const { id } = await Student.query().insert(data);

    res.status(201).send({ id }).end();
  }
});
/**
 * @api {patch} /student/:studentId Update partially student's info
 * @apiVersion 0.0.1
 * @apiName PatchStudent
 * @apiGroup Student
 * @apiParam (RequestBody) {String} email     Student's email.
 * @apiParam (RequestBody) {String} firstName Student's firstName.
 * @apiParam (RequestBody) {String} lastName  Student's lastName.
 * @apiParam (RequestBody) {Date}   birthDate Student's birthDate.
 * @apiParam (RequestBody) {String} gender    Student's gender.
 * @apiParam (RequestBody) {String} phone     Student's phone.
 * @apiParam (RequestBody) {String} country   Student's country.
 * @apiParam (RequestBody) {String} city      Student's city.
 * @apiParam (RequestBody) {String} district  Student's district.
 *
 */
studentRouter.patch('/student/:studentId', async (req, res) => {
  const data = req.body;
  const [student] = await Student.query().where('id', req.params.studentId);

  if (student) {
    await student.update(data);
    res.status(204).end();
  } else {
    res.status(412).end();
  }
});
/**
 * @api {delete} /student/:studentId Delete whole student's info
 * @apiVersion 0.0.1
 * @apiName DeleteStudent
 * @apiGroup Student
 * @apiParam {String} studentId Student's ID used to delete record.
 *
 */
studentRouter.delete('/student/:studentId', async (req, res) => {

  res.status(204).end();
});

module.exports = studentRouter;

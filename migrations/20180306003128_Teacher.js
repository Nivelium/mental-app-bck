exports.up = async (knex) => {
    return knex.schema.createTable('Teacher', (table) => {
        table.increments('id');
        table.string('email');
        table.string('firstName');
        table.string('lastName');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.string('birthDate');
        table.string('gender');
        table.string('phone');
        table.string('country');
        table.string('city');
        table.string('district');
        table.unique(['email', 'phone']);
    });
};

exports.down = async (knex) => {
    return knex.schema.dropTable('Teacher');
};
const assert = require('assert');
const config = require('config');
const dbConfig = config.get('knex');
const knex = require('knex')(dbConfig);
const appConfig = config.get('app');
const { request } = require('http');
const app = require('express')();

app.get('/', (req, res) => {
  res.send('Hello');
  res.end();
});
app.listen(appConfig.port);

const asyncRequest = function (opts, data) {
  return new Promise((resolve) => {
    const req = request(opts, (res) => {
      let body = '';

      res.on('data', (chunk) => {
        body += chunk;
      });
      res.on('end', () => {
        resolve(body);
      });
    });

    if (data) {
      req.write(data);
    }
    req.end();
  });
};

describe('Config', () => {
  describe('#knex()', () => {
    it('should return 1 if the connection established', async () => {
      const res = await knex.raw('select 1 as col');

      assert.equal(res.rows[0].col, 1);
    });
  });
  describe('#express', () => {
    it('should return Hello if the server is up', async () => {
      const options = {
        port: appConfig.port,
        method: 'GET',
        path: '/',
      };
      const res = await asyncRequest(options);

      assert.equal(res, 'Hello');
    });
  });
});

const Teacher = require('../../models/Teacher');
const assert = require('assert');

describe('Teacher', () => {
    const testData = [
        {
            'email': 'abrakadabra@tutu.ru',
            'firstName': 'Sasha',
            'lastName': 'Bugaga',
            'birthDate': '01.01.1900',
            'gender': 'M',
            'phone': '84955550505',
            'country': 'Uganda',
            'city': 'Karaganda',
            'district': 'Veselaya',
        },
        {
            'email': 'test2@tutu.ru',
            'firstName': 'Masha',
            'lastName': 'Dugaga',
            'birthDate': '01.01.2000',
            'gender': 'F',
            'phone': '84995550505',
            'country': 'Rwanda',
            'city': 'Lipeck',
            'district': 'Stremnaya',
        },
    ];

    before(async () => {
        return Teacher.query().insert(testData);
    });
    after(async () => {
        return Teacher.query().delete();
    });

    describe('#getAll()', () => {
        it('should return 2 teachers', async () => {
            const teachers = await Teacher.query();

            assert.equal(teachers.length, 2);
        });
        it('should return teacher with firstName = Sasha', async () => {
            const sasha = await Teacher.query().where('email', 'abrakadabra@tutu.ru');

            assert.equal(sasha[0].firstName, 'Sasha');
        });
    });
});

const Student = require('../../models/Student');
const assert = require('assert');

describe('Student', () => {
  const testData = [
    {
      'email': 'abrakadabra@tutu.ru',
      'firstName': 'Sasha',
      'lastName': 'Bugaga',
      'birthDate': '01.01.1900',
      'gender': 'M',
      'phone': '84955550505',
      'country': 'Uganda',
      'city': 'Karaganda',
      'district': 'Veselaya',
    },
    {
      'email': 'test2@tutu.ru',
      'firstName': 'Masha',
      'lastName': 'Dugaga',
      'birthDate': '01.01.2000',
      'gender': 'F',
      'phone': '84995550505',
      'country': 'Rwanda',
      'city': 'Lipeck',
      'district': 'Stremnaya',
    },
  ];

  before(async () => {
    return Student.query().insert(testData);
  });
  after(async () => {
    return Student.query().delete();
  });

  describe('#getAll()', () => {
    it('should return 2 students', async () => {
      const students = await Student.query();

      assert.equal(students.length, 2);
    });
    it('should return student with firstName = Sasha', async () => {
      const sasha = await Student.query().where('email', 'abrakadabra@tutu.ru');

      assert.equal(sasha[0].firstName, 'Sasha');
    });
  });
});

const assert = require('assert');
const supertest = require('supertest');
const app = require('express')();
const teacherRouter = require('../../routes/teacher');
const bodyParser = require('body-parser');
const _ = require('lodash');

app.use(bodyParser.json());
app.use('/testApi', teacherRouter);

describe('TeacherRouter', () => {
    const postData = {
        'email': 'teacherA@tutu.ru',
        'firstName': 'teacher',
        'lastName': 'A',
        'birthDate': '01.01.1900',
        'gender': 'M',
        'phone': '123456789',
        'country': 'Popo',
        'city': 'Tutu',
        'district': 'Piupiu',
    };
    const putData = {
        'email': 'teacherA@tutu.ru',
        'firstName': 'teacher',
        'lastName': 'A',
        'birthDate': '01.01.1900',
        'gender': 'M',
        'phone': '987654321',
        'country': 'Popo',
        'city': 'Tutu',
        'district': 'Piupiu',
    };
    const patchData = {
        'counry': 'MyOtherPopo',
    };

    const agent = supertest.agent(app);
    let postId;
    let putId;

    describe('POST /teacher', () => {
        it('Should return status = 201 and ID of created record', async () => {
            const { status, body } = await agent
                .post('/testApi/teacher')
                .send(postData);

            postId = body.id;

            assert.equal(status, 201);
        });
        it('Should return status = 412', async () => {
            const { status } = await agent
                .post('/testApi/teacher')
                .send(postData);

            assert.equal(status, 412);
        });
    });

    describe('PUT /teacher/:teacherId', () => {
        it('Should return status = 201 and ID of created record', async () => {
            const { status, body } = await agent
                .put(`/testApi/teacher/${postId + 1}`)
                .send(putData);

            putId = body.id;
            assert.equal(status, 201);
        });
        it('Should return status = 204', async () => {
            const { status } = await agent
                .put(`/testApi/teacher/${postId}`)
                .send(_.omit(putData, 'district'));

            assert.equal(status, 204);
        });
    });

    describe('PATCH /teacher/:teacherId', () => {
        it('Should return status = 204 and update the record made by postData', async () => {
            const { status } = await agent
                .patch(`/testApi/teacher/${postId}`)
                .send(patchData);

            assert.equal(status, 204);
        });
        it('Should return status = 412', async () => {
            const { status } = await agent
                .patch('/testApi/teacher/-1');

            assert.equal(status, 412);
        });
    });

    describe('GET /teacher/:teacherId', () => {
        it('Should return record postData', async () => {
            const { body, status } = await agent
                .get('/testApi/teacher/' + postId);

            assert.deepEqual({ body: _.omit(body, 'id', 'created_at'), status }, { body: postData, status: 200 });
        });
    });
});

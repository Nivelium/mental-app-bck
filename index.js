const config = require('config');
const app = require('express')();
const bunyan = require('bunyan');
const logger = bunyan.createLogger(config.get('bunyan'));
const appConfig = config.get('app');
const bodyParser = require('body-parser');
const studentRouter = require('./routes/student');
const teacherRouter = require('./routes/teacher');

app.use(bodyParser.json());
app.use('/', (req, res, next) => {
  req.log = logger;
  next();
});
app.use('/api/', studentRouter);
app.use('/api/', teacherRouter);

app.listen(appConfig.port);

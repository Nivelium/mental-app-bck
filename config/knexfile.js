// Update with your config settings.
process.env['NODE_CONFIG_DIR'] = __dirname;
const config = require('config');
const migrationConfig = {};

migrationConfig[process.env.NODE_ENV || 'development'] = config.get('knex');


module.exports = migrationConfig;
